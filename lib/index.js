"use strict";

require("regenerator-runtime/runtime.js");

require("core-js/modules/es.object.to-string.js");

require("core-js/modules/es.promise.js");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ajouterLigne(note) {
  var newLi = document.createElement("li");
  newLi.innerText = note.content;
  newLi.id = 'li' + note.id;
  var buttonDelete = document.createElement("button");
  buttonDelete.innerText = "Supprimer";
  buttonDelete.id = note.id;
  buttonDelete.addEventListener('click', function (event) {
    fetch("http://localhost:3000/notes/".concat(event.target.id), {
      method: 'DELETE'
    }).then(function (response) {
      return response.json();
    }).then(function (note) {
      document.querySelector("li#li".concat(event.target.id)).remove();
    });
  });
  newLi.appendChild(buttonDelete);
  return newLi;
}

function ajouterNote() {
  return _ajouterNote.apply(this, arguments);
}

function _ajouterNote() {
  _ajouterNote = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var newUl, raw, myHeaders, newNote, newLi;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            newUl = document.getElementById('myUl');
            raw = JSON.stringify({
              "content": document.getElementById('note').value
            });
            myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            _context.next = 6;
            return fetch("http://localhost:3000/notes", {
              method: 'POST',
              headers: myHeaders,
              body: raw
            }).then(function (response) {
              return response.json();
            });

          case 6:
            newNote = _context.sent;
            newLi = ajouterLigne(newNote);
            newUl.appendChild(newLi);
            document.getElementById('note').value = '';

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _ajouterNote.apply(this, arguments);
}

function chargerNotes() {
  return _chargerNotes.apply(this, arguments);
}

function _chargerNotes() {
  _chargerNotes = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
    var bodyElements, body, newUl, notes, index, note, newLi;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            bodyElements = document.getElementsByTagName('body');

            if (!(bodyElements.length > 0)) {
              _context2.next = 11;
              break;
            }

            body = bodyElements[0];
            newUl = document.createElement("ul");
            newUl.id = 'myUl';
            _context2.next = 7;
            return fetch("http://localhost:3000/notes", {
              method: 'GET'
            }).then(function (response) {
              return response.json();
            });

          case 7:
            notes = _context2.sent;

            for (index = 0; index < notes.length; index++) {
              note = notes[index];
              newLi = ajouterLigne(note);
              newUl.appendChild(newLi);
            }

            body.appendChild(newUl);
            document.getElementById('btn1').addEventListener('click', ajouterNote);

          case 11:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _chargerNotes.apply(this, arguments);
}

var result = chargerNotes().then().catch(function (error) {
  return console.log('error', error);
});