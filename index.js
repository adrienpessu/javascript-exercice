/**
 * Permet d'ajouter une note dans la liste
 * @param {} note 
 * @returns un element `li`
 */
 function ajouterLigne(note) {
    // crestion d'un élément li
    const newLi = document.createElement("li");
    // ajout de l'attribut content en tant que texte de l'élément li
    newLi.innerText = note.content;
    // ajout d'un identifiant
    newLi.id = 'li'+note.id;
    // creation d'un bouton
    const buttonDelete = document.createElement("button");
    buttonDelete.innerText = "Supprimer"; // ajout du texte au bouton
    buttonDelete.id = note.id; // ajout de l'identifiant de la note
    // ajout du listener sur l'evenement `click`
    buttonDelete.addEventListener('click', function(event) {
        // requête avec la méthode DELETE sur l'identifiant
        // de la cible de l'évènement, soit le bouton Supprimer
        fetch(`http://localhost:3000/notes/${event.target.id}`, {
            method: 'DELETE'
        })
        .then(response => {
            return response.json()
        })
        .then(note => {
            // après la suppression sur le server, on supprime l'element li
            document.querySelector(`li#li${event.target.id}`).remove()
        });
    })
    // Ajout du bouton supprimer à l'élément li
    newLi.appendChild(buttonDelete);
    return newLi;
}

// Recherche de l'élément `body`
const bodyElements = document.getElementsByTagName('body');
// On continu seulement si le body est trouvé
if (bodyElements.length > 0) {
    const body = bodyElements[0];
    // creation de l'élément `ul`
    const newUl = document.createElement("ul");
    // ajout d'identifiant 
    newUl.id = 'myUl';

    // Requête GET sur la liste des notes
    fetch("http://localhost:3000/notes", {
        method: 'GET'
    })
    .then(response => {
        // response correspond à la réponse Http complête 
        // ici, in récupère le corp de la réponse au format Json avec `.json()`
        return response.json()
    })
    .then(notes => {
        // Parcours des notes
        notes.forEach(
            note => {
                // création d'une ligne par note
                const newLi = ajouterLigne(note);
                newUl.appendChild(newLi);
            }
        );
        body.appendChild(newUl);
        
        // Ajout d'un listener sur le bouton avec l'identifiant `btn1`
        document.getElementById('btn1').addEventListener('click', function(){ 
            // on créé un objet avec un attribut `content` et la valeur du champ text.
            // on transforme ensuite cet objet en chaine de caractère
            const raw = JSON.stringify({
                "content": document.getElementById('note').value
              });

              // création de l'objet header
              // ajout du `Content-Type` pour indiquer au serveur 
              // que le corp de l'en-tête est au format json
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
    
            // requête avec la méthode POST
            // en y ajoutant l'en-tête et le corp 
            fetch("http://localhost:3000/notes", {
                method: 'POST',
                headers: myHeaders,
                body: raw
            })
            .then(response => {
                return response.json()
            })
            .then(note => {
                const newLi = ajouterLigne(note);
                newUl.appendChild(newLi);
            });

            document.getElementById('note').value = '';
        });
    })
    // en cas d'erreur dans la requête, on l'affiche dans la console
    .catch(error => console.error('error', error));

}




